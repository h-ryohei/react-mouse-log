import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Button from '@material-ui/core/Button';
const { detect } = require('detect-browser');
const browser = detect();

var id;
var array = [];
var intervalTimming = 6000;

class App extends Component {

  state = {
    mouseTracker: true,
    interval: true,
    count: 0,
    mouseX: 0,
    mouseY: 0,
  }

  onMouseMove = (e) => {
    this.setState({
      count: this.state.count + 1,
      mouseX: e.pageX,
      mouseY: e.pageY
    })

    var marray = [[this.state.count, Date.now(), this.state.mouseX, this.state.mouseY]]
    array = array.concat(marray);
    if (this.state.interval === false) {
      // console.log(this.state.count + " " + Date.now() + " (" + this.state.mouseX + ", " + this.state.mouseY + ")")
      console.log(array)
      array = []
    }
  }

　onInterval = () => {
    // console.log(this.state.count + " " + Date.now() + " (" + this.state.mouseX + ", " + this.state.mouseY + ")")
    console.log(array)
    array = []
  }

  removeInterval = () => {
    clearInterval(id)
    console.log(array)
    array = []
  }

  componentDidMount = () => {
    // MouseMove Event
    document.addEventListener('mousemove', this.onMouseMove)
    console.log("screen_size: (" + window.parent.screen.width + ", " + window.parent.screen.height + ")")
    switch (browser && browser.name) {
      case 'chrome':
      case 'safari':
        console.log("window_position: (" + window.screenLeft + ", " + window.screenTop + ")")
      break;

      case 'edge':
        console.log("body_position: (" + window.screenLeft + ", " + window.screenTop + ")")
      break;

      default:
        console.log('window_position: not supported');
    }
    console.log("window_size: (" + window.innerWidth + ", " + window.innerHeight + ")")

    // interval
    if (this.state.interval) {
      id = setInterval(this.onInterval, intervalTimming)
    }
  }

  componentWillUnmount = () => {
    document.removeEventListener('mousemove', this.onMouseMove)
  }

  handleTracker = () => {
    if (this.state.mouseTracker) {
      document.removeEventListener('mousemove', this.onMouseMove)
      clearInterval(id)
      console.log(array)
      array = []
    }else {
      document.addEventListener('mousemove', this.onMouseMove)
      id = setInterval(this.onInterval, intervalTimming)
    }

    this.setState({
      mouseTracker: this.state.mouseTracker ? false : true,
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <p>{ this.state.count }</p>
        <p>{ this.state.mouseX }, { this.state.mouseY }</p>
        <Button variant="contained" color="primary" onClick={this.handleTracker}>
          Mouse Tracker
        </Button>
        <Button variant="contained" color="primary" onClick={this.removeInterval}>
          Stop Interval
        </Button>
      </div>
    );
  }
}

export default App;
